from fastapi import APIRouter
from utils.jwt_manager import create_token
from fastapi.responses import JSONResponse
from schemas.user import User

user_router = APIRouter()

#Endpoint para el login del usuario y recibir la llave de autentificacion
@user_router.post('/login', tags=['auth'])
def login(user: User):
     if user.email == "admin@gmail.com" and user.password == "admin":
          token: str = create_token(dict(user))
          return JSONResponse(status_code=200, content=token)