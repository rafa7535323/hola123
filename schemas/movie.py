from pydantic import BaseModel, Field
from typing import Optional
class Movie(BaseModel):
#     id: int | None=None	Otra forma de hacerlo
    id: Optional [int]= None
    Title: str =Field(default="Titulo de la pelicula", min_length=5, max_length=50)
    Overview: str =Field(default="Sinopsis")
    Year: int =Field(le= 2024)
    Rating: float =Field(ge= 0.0, le=10)
    Category: str =Field(min_length=1, max_length=25)

    class Config:
        json_schema_extra ={
            "Example": {
                    "Title": "Mi pelicula",
                    "Overview": "Sinopsis de pelicula",
                    "Year": 2024,
                    "Rating": 6.6,
                    "Category":"Drama, Accion"
                }
        }