from pydantic import BaseModel

#Clase para el login del usuario
class User(BaseModel):
    email: str
    password: str