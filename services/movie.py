from models.movie import Movie  as MovieModel
from schemas.movie import Movie

class   MovieService():

    def __init__(self, db) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result
    
    def get_movie_id(self, id: int):
        result=self.db.query(MovieModel).filter(MovieModel.id==id).first()
        return result
    
    def get_movie_category(self, Category: str):
        result=self.db.query(MovieModel).filter(MovieModel.Category==Category).all()
        return result
    
    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.model_dump())
        self.db.add(new_movie)
        self.db.commit()
        return new_movie
    
    def update_movie(self, id: int, movie: Movie):
        update_movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        update_movie.Title=movie.Title
        update_movie.Overview=movie.Overview
        update_movie.Year=movie.Year
        update_movie.Rating=movie.Rating
        update_movie.Category=movie.Category
        self.db.commit()
        return update_movie

    def delete_movie(self, id:int):
        self.db.query(MovieModel).filter(MovieModel.id== id).delete()
        self.db.commit()
        return